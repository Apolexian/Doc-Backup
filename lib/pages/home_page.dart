import 'package:docshare/models/doc.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import '../services/authentication.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:io';
import '../widgets/doc_card.dart';
import 'package:path/path.dart';
import 'package:intl/intl.dart';

/// Home page is the main page for uploading docs
/// the add button is used to browse PDF files to
/// add, then upload is pressed to upload them
/// to firestore.
/// Additionally, a MyDoc database entry is made
/// for each document uploaded.
class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final databaseReference = FirebaseDatabase.instance.reference();
  final userDbRef = FirebaseDatabase.instance.reference().child(user.uid);
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  File uploadDoc;
  static FirebaseUser user;

  final _textEditingController = TextEditingController();
  StreamSubscription<Event> _onTodoAddedSubscription;
  StreamSubscription<Event> _onTodoChangedSubscription;

  @override
  void initState() {
    super.initState();
    initUser();
  }

  /// Fetch current user
  initUser() async {
    user = await _auth.currentUser();
    setState(() {});
  }

  /// Dispose on cancel
  @override
  void dispose() {
    _onTodoAddedSubscription.cancel();
    _onTodoChangedSubscription.cancel();
    super.dispose();
  }

  /// Used to sign the user out
  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  /// Uses FilePicker to pick pdf and save it
  /// in uploadDoc variable
  Future getDoc() async {
    var tempDoc = await FilePicker.getFile(
        type: FileType.custom, allowedExtensions: ['pdf']);

    setState(() {
      uploadDoc = tempDoc;
    });
  }

  /// interface used to upload the pdf
  Widget enableUpload() {
    return Container(
      child: Column(
        children: <Widget>[
          DocRow(uploadDoc.path, "PDF"),
          RaisedButton(
            elevation: 7.0,
            child: Text('Upload'),
            textColor: Colors.white,
            color: Colors.blue,
            onPressed: () {
              final StorageReference firebaseStorageRef = FirebaseStorage
                  .instance
                  .ref()
                  .child(basename(uploadDoc.path));
              final StorageUploadTask task =
                  firebaseStorageRef.putFile(uploadDoc);
              saveDocRef(task);
            },
          ),
          Text("Your Docs"),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Doc Upload'),
        centerTitle: true,
      ),
      body: new Center(
        child: uploadDoc == null ? Text('Select a PDF') : enableUpload(),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: getDoc,
        tooltip: 'Add Doc',
        child: new Icon(Icons.add),
      ),
    );
  }

  void saveDocRef(StorageUploadTask task) async {
    // fill in data for the Doc
    // userID is id of user that saved the doc
    // docURL can be used to retrieve the doc from
    // firestore, docName is name of the doc
    // also save stamp of when doc was uploaded
    var storageTaskSnapshot = await task.onComplete;
    var userID = user.uid;
    var docURL = await storageTaskSnapshot.ref.getDownloadURL();
    var docName = basename(uploadDoc.path);
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd-MM-yyyy').format(now);
    MyDoc saveDoc = new MyDoc();
    saveDoc.createRecord(
        databaseReference, userID, docURL, docName, formattedDate);
  }
}
