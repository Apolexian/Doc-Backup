import 'package:flutter/material.dart';

class DocRow extends StatelessWidget {
  final String titleText;
  final String subtitleText;

  const DocRow(this.titleText, this.subtitleText);

  Widget _myListTitle(String title, String subtitle) {
    return ListTile(
      leading: Icon(Icons.file_upload, size: 50),
      title: Text(title),
      subtitle: Text(subtitleText),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_myListTitle(titleText, subtitleText)],
      ),
    );
  }
}
