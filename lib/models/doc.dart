import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';

/// Represents the info about an uploaded pdf
/// userID - user than uploaded the pdf
/// docURL - the url of the doc in firestore
/// docName - name of the doc in firestore
/// stamp - when the doc was uploaded to firestore
class MyDoc {
  String userID;
  String docURL;
  String docName;
  String stamp;

  MyDoc({
    String userID,
    this.docURL,
    this.docName,
    this.stamp,
  });

  factory MyDoc.fromJson(Map<String, dynamic> json) => new MyDoc(
        userID: json["userId"],
        docURL: json["docURL"],
        docName: json["docName"],
        stamp: json["stamp"],
      );

  Map<String, dynamic> toJson() => {
        "userID": userID,
        "docURL": docURL,
        "docName": docName,
        "stamp": stamp,
      };

  factory MyDoc.fromDocument(DocumentSnapshot doc) {
    return MyDoc.fromJson(doc.data);
  }

  void createRecord(DatabaseReference databaseReference, String userID,
      String docURL, String docName, String stamp) {
    var key = docName.replaceAll(new RegExp(r'[^\w]+'), '');
    databaseReference.child("docs").child(userID).child(key).set({
      'userID': userID,
      'docURL': docURL,
      'docName': docName,
      'stamp': stamp,
    });
  }
}

MyDoc myDocFromJson(String str) {
  final jsonData = json.decode(str);
  return MyDoc.fromJson(jsonData);
}

String myDocToJson(MyDoc data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
